define('saucy', [
    'jquery',
    'underscore',
    'exports'
], function(
    $,
    _,
    exports) {

    var popups = {};

    function classify(name) {
        return name.toLowerCase().replace(/[^a-zA-Z]/gi, "-");
    }

    function displayPane(id, contentClass, width, height, title) {
        var popup = popups[id];
        if (!popup) {
            popup = popups[id] = new AJS.Dialog({
                width: width, 
                height: height,
                id: id,
                closeOnOutsideClick: false
            });
            popup.addHeader(title);
            popup.addCancel("Close", function() {
                popup.hide();
            });        
            popup.addPanel("", "", contentClass);            
        }
        popup.show();
        return $("." + contentClass).empty();
    }

    function encodePathSegments(path) {
        return _.map(path.split("/"), function (component) {
            return encodeURIComponent(component);
        }).join('/');
    }

    function loadVis(path, mode, $contentPane, vis) {        
        $contentPane.spin('large');
        $.ajax(AJS.contextPath() + "/rest/saucy-charts/1.0/projects/" +
                  encodeURIComponent(path.pKey) + "/repos/" + encodeURIComponent(path.repo) + "/" +
                  encodeURIComponent(mode) + "/" + encodePathSegments(path.path), {
            data: {
                at: path.at
            },
            error: function(xhr, status, error) {
                $contentPane.empty().text(status + " " + error);
            },
            success: function(data) {
                $contentPane.empty();
                vis(data);
            }

        });
    }

    function rank(val) {
        var suffix;
        var mod100 = val % 100;
        if (mod100 == 11 || mod100 == 12) {
            suffix = "th"; // eleventh, twelfth
        } else if (val % 10 === 1) {
            suffix = "st";
        } else if (val % 10 === 2) {
            suffix = "nd";
        } else {
            suffix = "th";
        }
        return val + suffix;
    }

    exports.popupWordCloud = function(path) {      
        var $contentPane = displayPane("word-cloud", "word-cloud-content", 1050, 750, path.path);
        loadVis(path, "words", $contentPane, function(data) {
            exports.drawWordCloud(".word-cloud-content", data, {
                w: 1000,
                h: 590
            });
        });
    };

    // mmmmmm.. blame pie
    exports.popupBlamePie = function(path) {
        var $contentPane = displayPane("blame-pie", "blame-pie-content", 800, 600, path.path);
        loadVis(path, "blame", $contentPane, function(data) {
            exports.drawBlamePie(".blame-pie-content", data, {
                w: 750,
                h: 445
            });
        });
    };

    exports.popupBlameHistory = function(path) {
        var $contentPane = displayPane("blame-history", "blame-history-content", 1050, 750, path.path);
        loadVis(path, "blame-history", $contentPane, function(data) {
            exports.drawBlameHistory(".blame-history-content", data, {
                w: 1000,
                h: 590
            });
        });
    };

    exports.popupLocHistory = function(path) {
        var $contentPane = displayPane("loc-history", "loc-history-content", 1050, 750, path.path);
        loadVis(path, "loc-history", $contentPane, function(data) {
            exports.drawLocHistory(".loc-history-content", data, {
                w: 1000,
                h: 590
            });
        });
    };

    exports.drawLocHistory = function(target, locHistory, opts) {
        opts = $.extend({
            w: 1000,
            h: 590
        }, opts);

        var layers = [];
        var byAuthor = {};
        var timestamps = [];
        var authors = [];

        var maxAdded = 0;
        var maxRemoved = 0;

        _.each(locHistory, function(snapshot, snapshotIdx) {
            timestamps.push(snapshot.timestamp);
            _.each(snapshot.blame, function(authorSnapshot) {
                var values = byAuthor[authorSnapshot.author];
                if (values === undefined) {
                    // new author: populate with 0s for preceding snapshots
                    values = [];
                    for (var i = 0; i < snapshotIdx; i++) {
                        values.push({
                            "x": i,
                            "added": 0,
                            "removed": 0
                        });
                    }
//                    layer = {
//                        name: authorSnapshot.author,
//                        values: values
//                    };
                    byAuthor[authorSnapshot.author] = values;
                    layers.push(values);
                    authors.push(authorSnapshot.author);
                }
                // add blame for author for this snapshot
                values.push({
                    "x": snapshotIdx,
                    "added": authorSnapshot.added,
                    "removed": authorSnapshot.removed
                });

                // TODO only really need to do this on the last snapshot..
                if (authorSnapshot.added > maxAdded) {
                    maxAdded = authorSnapshot.added;
                }
                if (authorSnapshot.removed < maxRemoved) {
                    maxRemoved = authorSnapshot.removed;
                }
            });

            // populate with 0s for authors who have zero blame at this snapshot
            var expectedLength = snapshotIdx + 1;
            _.each(layers, function(values) {
                while (values.length < expectedLength) {
                    values.push({
                        "x": values.length - 1,
                        "added": 0,
                        "removed": 0
                    });
                }
            });
        });

        console.log(layers);

        // line chart generation code inspired by the example at http://bl.ocks.org/mbostock/3883245

        var x = d3.time.scale()
            .range([0, opts.w])
            .domain([0, layers[0].length]); // all series are the same length

        var y = d3.scale.linear()
            .range([opts.h - 10, 0])
            .domain([maxRemoved, maxAdded]);

//        var xAxis = d3.svg.axis()
//            .scale(x)
//            .orient("bottom");
//
//        var yAxis = d3.svg.axis()
//            .scale(y)
//            .orient("left");

        var addedLine = d3.svg.line()
            .interpolate("basis")
            .x(function(d) {
                return x(d.x);
            })
            .y(function(d) {
                return y(d.added);
            });

        var removedLine = d3.svg.line()
            .interpolate("basis")
            .x(function(d) {
                return x(d.x);
            })
            .y(function(d) {
                return y(d.removed);
            });

        var baseLine = d3.svg.line()
            .x(function(d) {
                return x(d.x);
            })
            .y(function(d) {
                return y(0);
            });

        var svg = d3.select(target).append("svg")
            .style("cursor", "crosshair")
            .attr("width", opts.w)
            .attr("height", opts.h)
            .append("g")
            .attr("transform", "translate(" + 10 + "," + 5 + ")"); // margins

//        svg.append("g")
//            .attr("class", "x axis")
//            .attr("transform", "translate(0," + opts.h + ")")
//            .call(xAxis);
//
//        svg.append("g")
//            .attr("class", "y axis")
//            .call(yAxis)
//            .append("text")
//            .attr("transform", "rotate(-90)")
//            .attr("y", 6)
//            .attr("dy", ".71em")
//            .style("text-anchor", "end")
//            .text("LOC");

        var color = d3.scale.category10();

        svg.selectAll("path.line-added")
            .data(layers)
            .enter()
            .append("path")
            .attr("class", "line-added")
            .attr("data-author", function(d, i) {
                return authors[i];
            })
            .attr("data-author-index", function(d, i) {
                return i;
            })
            .attr("class", function(d, i) {
                return "line-added " + classify(authors[i]);
            })
            .attr("stroke", function (d, i) {
                 return color(i);
            })
            .attr("d", removedLine)
            .attr("opacity", 1)
            .transition()
            .duration(1500)
            .ease("elastic")
            .attr("d", addedLine)
            .each("end", function() {
                svg.selectAll("path.line-removed")
                    .data(layers)
                    .enter()
                    .append("path")
                    .attr("class", function(d, i) {
                        return "line-removed " + classify(authors[i]);
                    })
                    .attr("stroke", function (d, i) {
                        return color(i);
                    })
                    .attr("d", addedLine)
                    .transition()
                    .duration(2000)
                    .ease("bounce")
                    .attr("d", removedLine)
                    .each("end", function() {

                        // DEMO IN 10 MIN COPY-PASTE CODE

                        var allPaths = svg.selectAll("path").on("mouseover", function() {
                            // highlight this later (if not already highlit) and fade others
                            var $this = d3.select(this);
                            var author = $this.attr("data-author");
                            var $joint = d3.selectAll("." + classify(author));
                            var alreadyOpaque = $this.attr("opacity") == 1;
                            allPaths.transition(0); // cancel current transition
                            allPaths.transition(150).attr("opacity", .2);
                            $joint.transition(0); // cancel the current transition
                            if (alreadyOpaque) {
                                $joint.attr("opacity", 1);
                            } else {
                                $joint.transition(250).attr("opacity", 1);
                            }
//                            $this.transition(250).attr("stroke-width", 10);
                            infoBox.transition(250).attr("opacity",.8);
                        })
                        .on("mouseout", function() {
                            // return all pie slices & the legend to normal visibility
                            allPaths.transition(0); // cancel current transition
                            allPaths.transition(250).attr("opacity", 1);
//                            allPaths.transition(250).attr("stroke-width", 3);
                            d3.selectAll("g.author-info").transition(100).attr("opacity", 0);
                            d3.selectAll(".legend").transition(250).attr("opacity", 1);
                            infoBox.transition(100).attr("opacity", 0);
                        })
                        .on("mousemove", function() {
                            var $this = d3.select(this);
                            var author = $this.attr("data-author");
                            var authorIndex = $this.attr("data-author-index");
                            var values = byAuthor[author];

                            var x = Math.floor((d3.mouse(this)[0] / opts.w * values.length));
                            var added;
                            var removed;
                            var index;
                            for (var i = 1; i < values.length; i++) {
                                if ((values[i-1].x <= x && x < values[i].x) || i == values.length - 1) {
                                    added = values[i-1].added;
                                    removed = values[i-1].removed;
                                    index = i - 1;
                                    break;
                                }
                            }
                            var date = new Date(timestamps[x] * 1000);
                            var dateStr = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();

                            // populate info box
                            infoSquare.attr("fill", color(authorIndex));
                            infoName.text(author);
                            infoDate.text(dateStr);
                            infoLoc.text("LOC: +" + added + " " + removed);
                        });
                    });
            });

        // DEMO IN 30 MIN COPY-PASTE CODE

        var infoBox = svg
            .append("g")
            .attr("class", "info-box")
            .attr("opacity", 0);

        // infobox background
        infoBox.append("rect")
            .attr("x", opts.w - 210)
            .attr("y", 10)
            .attr("width", 200)
            .attr("height", 80)
            .attr("fill", "#FFFFFF")
            .attr("stroke", "#000000")
            .attr("stroke-width", "1");

        var infoSquare = infoBox.append("rect")
            .attr("x", opts.w - 190)
            .attr("y", 25)
            .attr("width", 10)
            .attr("height", 10)
            .attr("fill", "#FF0000");

        var infoName = infoBox.append("text")
            .text("Tim Pettersen")
            .attr("x", opts.w - 175)
            .attr("y", 35)
            .attr("fill", "black")
            .attr("font-family", "sans-serif")
            .attr("font-size", "14px");

        var infoDate = infoBox.append("text")
            .text("20 Dec 1984")
            .attr("x", opts.w - 190)
            .attr("y", 55)
            .attr("fill", "black")
            .attr("font-family", "sans-serif")
            .attr("font-size", "14px");

        var infoLoc = infoBox.append("text")
            .text("1234 (57%)")
            .attr("x", opts.w - 190)
            .attr("y", 75)
            .attr("fill", "black")
            .attr("font-family", "sans-serif")
            .attr("font-size", "14px");

    };

    exports.drawBlameHistory = function(target, blame, opts) {
        opts = $.extend({
            w: 1000,
            h: 590
        }, opts);

        // transform blame history snapshots into per-author layers
        var layers = [];
        var byAuthor = {};
        var timestamps = [];
        var locSnapshots = [];
        var maxFileLoc = 0;

        _.each(blame, function(snapshot, snapshotIdx) {
            var snapshotLoc = 0;
            timestamps.push(snapshot.timestamp);
            _.each(snapshot.blame, function(authorSnapshot) {
                var layer = byAuthor[authorSnapshot.author];
                if (layer === undefined) {
                    // new author: populate with 0s for preceding snapshots
                    var values = [];
                    for (var i = 0; i < snapshotIdx; i++) {
                        values.push({
                            "x": i,
                            "y": 0
                        });
                    }
                    layer = {
                        name: authorSnapshot.author,
                        values: values
                    };
                    byAuthor[layer.name] = layer;
                    layers.push(layer);
                }
                // add blame for author for this snapshot
                layer.values.push({
                    "x": snapshotIdx,
                    "y": authorSnapshot.loc
                });
                snapshotLoc += authorSnapshot.loc;
            });

            // populate with 0s for authors who have zero blame at this snapshot
            var expectedLength = snapshotIdx + 1;
            _.each(layers, function(layer) {
                var values = layer.values;
                while (values.length < expectedLength) {
                    values.push({
                        "x": values.length - 1,
                        "y": 0
                    });
                }
            });

            locSnapshots.push(snapshotLoc);

            if (snapshotLoc > maxFileLoc) {
                maxFileLoc = snapshotLoc;
            }
        });

        // handle single commit/committer files
        if (layers.length === 1 || layers[0].values.length === 1) {
            d3.select(target)
                .append("svg")
                .attr("width", opts.w)
                .attr("height", opts.h)
                .append("text")
                    .attr("x", opts.w / 2)
                    .attr("y", opts.h / 2)
                    .attr("text-anchor", "middle")
                    .attr("font-size", 48)
                    .style("cursor", "default")
                    .text("100% " + layers[0].name);
            return;
        }

        var stack = d3.layout.stack()
            .offset("silhouette")
            .values(function(d) {
                return d.values;
            });

        var stackedLayers = stack(layers);

        var x = d3.scale.linear()
            .domain([0, timestamps.length - 1])
            .range([0, opts.w]);
        var y = d3.scale.linear()
            .domain([0, maxFileLoc])
            .range([0, opts.h]);

        var color = d3.scale.category10();

        var area = d3.svg.area()
            .interpolate("basis")
            .x(function(d) {
                return x(d.x);
            })
            .y0(function(d) {
                return y(d.y0);
            })
            .y1(function(d) {
                return y(d.y0 + d.y);
            });

        var svg = d3.select(target)
            .append("svg")
            .style("cursor", "crosshair")
            .attr("width", opts.w)
            .attr("height", opts.h);

        var paths = svg.selectAll("path")
            .data(stackedLayers)
            .enter()
            .append("path")
            .attr("d", function (d) {
                return area(d.values);
            })
            .attr("data-author", function (d) {
                return d.name;
            })
            .attr("data-author-index", function (d, i) {
                return i;
            })
            .style("fill", function(d, i) {
                return color(i);
            })
            .attr("opacity", 1)
            .on("mouseover", function() {
                // highlight this later (if not already highlit) and fade others
                var $this = d3.select(this);
                var alreadyOpaque = $this.attr("opacity") == 1;
                paths.transition(0); // cancel current transition
                paths.transition(150).attr("opacity", .2);
                $this.transition(0); // cancel the current transition
                if (alreadyOpaque) {
                    $this.attr("opacity", 1);
                } else {
                    $this.transition(250).attr("opacity", 1);
                }
                infoBox.transition(250).attr("opacity",.8);
            })
            .on("mouseout", function() {
                // return all pie slices & the legend to normal visibility
                paths.transition(0); // cancel current transition
                paths.transition(250).attr("opacity", 1);
                d3.selectAll("g.author-info").transition(100).attr("opacity", 0);
                d3.selectAll(".legend").transition(250).attr("opacity", 1);
                infoBox.transition(100).attr("opacity", 0);
            })
            .on("mousemove", function() {
                var $this = d3.select(this);
                var author = $this.attr("data-author");
                var authorIndex = $this.attr("data-author-index");
                var values = byAuthor[author].values;

                var x = Math.floor((d3.mouse(this)[0] / opts.w * values.length));
                var loc;
                var index;
                for (var i = 1; i < values.length; i++) {
                    if ((values[i-1].x <= x && x < values[i].x) || i == values.length - 1) {
                        loc = values[i-1].y;
                        index = i - 1;
                        break;
                    }
                }
                var date = new Date(timestamps[x] * 1000);
                var dateStr = date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();

                var percentage = (loc / locSnapshots[x] * 100).toFixed(2) + "%";

                // populate info box
                infoSquare.attr("fill", color(authorIndex));
                infoName.text(author);
                infoDate.text(dateStr);
                infoLoc.text(loc + " LOC (" + percentage + ")");
            });

        var infoBox = svg
            .append("g")
            .attr("class", "info-box")
            .attr("opacity", 0);

        // infobox background
        infoBox.append("rect")
            .attr("x", opts.w - 210)
            .attr("y", 10)
            .attr("width", 200)
            .attr("height", 80)
            .attr("fill", "#FFFFFF");

        var infoSquare = infoBox.append("rect")
            .attr("x", opts.w - 190)
            .attr("y", 25)
            .attr("width", 10)
            .attr("height", 10)
            .attr("fill", "#FF0000");

        var infoName = infoBox.append("text")
            .text("Tim Pettersen")
            .attr("x", opts.w - 175)
            .attr("y", 35)
            .attr("fill", "black")
            .attr("font-family", "sans-serif")
            .attr("font-size", "14px");

        var infoDate = infoBox.append("text")
            .text("20 Dec 1984")
            .attr("x", opts.w - 190)
            .attr("y", 55)
            .attr("fill", "black")
            .attr("font-family", "sans-serif")
            .attr("font-size", "14px");

        var infoLoc = infoBox.append("text")
            .text("1234 (57%)")
            .attr("x", opts.w - 190)
            .attr("y", 75)
            .attr("fill", "black")
            .attr("font-family", "sans-serif")
            .attr("font-size", "14px");
    };

    exports.drawBlamePie = function(target, blame, opts) {
        opts = $.extend({
            w: 1000,
            h: 600,
            r: 220,
            maxAngle: 45
        }, opts);

        // handle single author
        if (blame.length === 1) {
            d3.select(target)
                .append("svg")
                .attr("width", opts.w)
                .attr("height", opts.h)
                .append("text")
                .attr("x", opts.w / 2)
                .attr("y", opts.h / 2)
                .attr("text-anchor", "middle")
                .attr("font-size", 48)
                .style("cursor", "default")
                .text("100% " + blame[0].author);
            return;
        }
        
        var r = opts.r, 
            color = d3.scale.category20();     //builtin range of colors

        blame.sort(function(a, b) {
            return b.loc - a.loc;
        });

        // collapse 13th+ most frequent committers into "Other" category
        var other = {author:"Other", loc: 0};
        var total = 0;
        blame = blame.filter(function(elem, idx) {
            total += elem.loc;
            if (idx < 12) {
                return true;
            } else {
                other.loc += elem.loc;
                return false;
            }
        });
        if (other.loc > 0) {
            blame.push(other);
        }

        var data = _.map(blame, function(d) {
            return {
                author: d.author,
                loc: (d.loc / total) * 100
            };
        });
                 
        // pie chart generation code inspired by the excellent example at https://gist.github.com/enjalot/1203641

        var svg = d3.select(target)
            .append("svg:svg")
            .data([data])
            .attr("width", opts.w)
            .attr("height", opts.h)
            .style("cursor", "crosshair");

        var vis = svg.append("svg:g")
                     .attr("transform", "translate(" + r + "," + r + ")")
     
        var arc = d3.svg.arc()
            .outerRadius(r);
     
        var pie = d3.layout.pie()
            .sort(null) // pre-sorted
            .value(function(d) { return d.loc; });
     
        var arcs = vis.selectAll("g.slice")
            .data(pie)                     
            .enter()                       
                .append("g")
                .attr("class", "slice")
                .attr("data-author", function(d) {
                    return d.data.author;
                })
                .attr("fill-opacity", 1)
                .on("mouseover", function() {
                    // highlight this pie slice (if not already highlit) and fade others
                    var $this = d3.select(this);
                    var alreadyOpaque = $this.attr("fill-opacity") == 1;
                    arcs.transition(0); // cancel current transition
                    arcs.transition(150).attr("fill-opacity", .15);
                    $this.transition(0); // cancel the current transition
                    if (alreadyOpaque) {
                        $this.attr("fill-opacity", 1);
                        $this.attr("opacity", 1);
                    } else {
                        $this.transition(250).attr("fill-opacity", 1);
                    }
                    // hide the legend
                    d3.selectAll(".legend").transition(150).attr("opacity", 0);

                    // display the author-info
                    var author = $(this).attr("data-author");
                    d3.selectAll("g.author-info").each(function() {
                        var $this = d3.select(this);
                        if ($this.attr("data-author") === author) {
                            $this.transition(250).attr("opacity", 1);
                        }
                    });
                })
                .on("mouseout", function() {
                    // return all pie slices & the legend to normal visibility
                    arcs.transition(0); // cancel current transition
                    arcs.transition(250).attr("fill-opacity", 1);
                    d3.selectAll("g.author-info").transition(100).attr("opacity", 0);
                    d3.selectAll(".legend").transition(250).attr("opacity", 1);
                });
     
        arcs.append("path")
                .attr("fill", function(d, i) { return color(i); })
                .attr("d", arc);

        // legend

        var legend = svg.append("g")
                        .attr("class", "legend");

        legend.selectAll("rect")
              .data(blame)
              .enter()
              .append("rect")
              .attr("x", r * 2 + 50)
              .attr("y", function(d, i) {
                  return 50 + i * 30;
              })
              .attr("width", 10)
              .attr("height", 10)
              .attr("fill", function(d, i) {
                  return color(i);
              });

        legend.selectAll("text")
            .data(blame)
            .enter()
            .append("text")
            .text(function (d) {
              return d.author;
            })
            .attr("x", r * 2 + 65)
            .attr("y", function(d, i) {
                return 60 + i * 30;
            })                
            .attr("fill", "black")
            .attr("font-family", "sans-serif")
            .attr("font-size", "14px");

        // author info

        var authorInfo = svg.selectAll("g.author-info")
            .data(blame)
            .enter()
            .append("g")
            .attr("class", "author-info")
            .attr("data-author", function (d) {
                return d.author;
            })
            .attr("opacity", 0); // start hidden

        authorInfo.append("rect")
            .attr("x", r * 2 + 50)
            .attr("y", 50)
            .attr("width", 10)
            .attr("height", 10)
            .attr("fill", function(d, i) {
                return color(i);
            });

        authorInfo.append("text")
            .text(function (d) {
                return d.author;
            })
            .attr("x", r * 2 + 65)
            .attr("y", 60)
            .attr("fill", "black")
            .attr("font-family", "sans-serif")
            .attr("font-size", "14px");

        authorInfo.append("text")
            .text(function (d) {
                return d.loc + " LOC";
            })
            .attr("x", r * 2 + 65)
            .attr("y", 59 + 30)
            .attr("fill", "black")
            .attr("font-family", "sans-serif")
            .attr("font-size", "14px");

        authorInfo.append("text")
            .text(function (d) {
                return (100 * d.loc / total).toFixed(2) + " %";
            })
            .attr("x", r * 2 + 65)
            .attr("y", 59 + 60)
            .attr("fill", "black")
            .attr("font-family", "sans-serif")
            .attr("font-size", "14px");

        authorInfo.append("text")
            .text(function (d, idx) {
                if (d.author === "Other") {
                    return ""; // don't show rank for other
                }
                return "Rank: " + rank(idx + 1);
            })
            .attr("x", r * 2 + 65)
            .attr("y", 59 + 90)
            .attr("fill", "black")
            .attr("font-family", "sans-serif")
            .attr("font-size", "14px");
    };

    exports.drawWordCloud = function(target, words, opts) {
        opts = $.extend({
            w: 1000,
            h: 600,
            maxAngle: 45
        }, opts);

        var w = opts.w;
        var h = opts.h;
        var maxAngle = opts.maxAngle;

        var fill = d3.scale.category20();

        var max = 0;
        _.each(words, function(word) {
          if (word.count > max) {
            max = word.count;
          }
        });
        max = Math.log(max);

        words = words.map(function(d) {
          return {text: d.word, size: (Math.log(d.count) / max) * 100};
        });

        // word cloud generation code based on the excellent tutorial at http://www.jasondavies.com/wordcloud/#http%3A%2F%2Fsearch.twitter.com%2Fsearch.json%3Frpp%3D100%26q%3D%7Bword%7D=cloud

        d3.layout.cloud().size([w, h])
          .words(words)
          .rotate(function(d) { return (Math.random() - Math.random()) * maxAngle; })
          .font("Impact")
          .fontSize(function(d) { return d.size; })
          .on("end", draw)
          .start();

        function draw(words) {
          d3.select(target).append("svg")
              .attr("width", w)
              .attr("height", h)
            .append("g")
              .attr("transform", "translate(" + w / 2 + "," + h / 2 + ")")
            .selectAll("text")
              .data(words)
            .enter().append("text")
              .style("font-size", function(d) { return d.size + "px"; })
              .style("font-family", "Impact")
              .style("fill", function(d, i) { return fill(i); })
              .attr("text-anchor", "middle")
              .attr("transform", function(d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
              })
              .text(function(d) { return d.text; });
        }
    };
});

AJS.$(document).ready(function() {
    function currentPath() {
        var url = location.pathname.match(/\/projects\/(.*)\/repos\/(.*)\/browse\/(.*)(?:$|\?)/);
        var at = "HEAD";
        var ats = location.pathname.match(/[\?&]at=([^&]+)/);
        if (ats && ats.length > 0) {
          at = ats[0];
        }
        return {
            pKey: url[1],
            repo: url[2], 
            path: url[3], 
            at: at
        };        
    }

    var $dropTrigger = AJS.$("<a href='#saucy-charts' aria-owns='saucy-charts-dd' aria-haspopup='true' class='aui-button aui-dropdown2-trigger aui-style-default'>Charts</a>");
    var $dropContainer = AJS.$("<div id='saucy-charts-dd' class='aui-dropdown2 aui-style-default'>");
    var $dropList = AJS.$("<ul class='aui-list-truncate'>");
    $dropContainer.append($dropList);

    var saucy = require('saucy');

    AJS.$.each({
        "Word cloud": saucy.popupWordCloud,
        "Blame pie": saucy.popupBlamePie,
        "Blame history": saucy.popupBlameHistory,
        "Added and removed": saucy.popupLocHistory
    }, function(name, visFunc) {
        var a = AJS.$("<a>" + name + "</a>");
        a.click(function() {
            visFunc(currentPath());
        });
        var li = AJS.$("<li>").append(a);
        $dropList.append(li);
    });

    AJS.$(".source-commands .aui-toolbar2-primary")
        .append($dropTrigger)
        .append($dropContainer)
});
