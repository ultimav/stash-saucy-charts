package com.atlassian.stash.saucycharts;

import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.util.Pair;

import java.util.Map;

public interface SaucyService {

    Map<String, Integer> getWords(Repository repository, String path, String at);

    Map<String, Integer> getBlame(Repository repository, String path, String at);

    Map<Long, Map<String, Integer>> getBlameHistory(Repository repository, String path, String at, int segments);

    Map<Long, Map<String, Pair<Integer, Integer>>> getLocHistory(Repository repository, String path, String at, int segments);

}
