package com.atlassian.stash.saucycharts.internal.rest.model;

import com.atlassian.stash.rest.data.RestMapEntity;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class AuthorLoc extends RestMapEntity {

    // blame
    public AuthorLoc(String author, int loc) {
        put("author", author);
        put("loc", loc);
    }

    // numstat
    public AuthorLoc(String author, int added, int removed) {
        put("author", author);
        put("added", added);
        put("removed", removed);
    }

}
