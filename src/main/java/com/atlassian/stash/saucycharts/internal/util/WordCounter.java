package com.atlassian.stash.saucycharts.internal.util;

import java.util.Map;

public interface WordCounter {

    void add(String word);

    void remove(String word);

    Map<String, Integer> getCounts();

    void clear();

    boolean isEmpty();

}
