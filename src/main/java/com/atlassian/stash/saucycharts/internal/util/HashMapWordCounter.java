package com.atlassian.stash.saucycharts.internal.util;

import com.google.common.collect.Maps;

import java.util.Map;

public class HashMapWordCounter implements WordCounter {

    private final Map<String, IntHolder> map = Maps.newHashMap();

    public void add(String word) {
        IntHolder intHolder = map.get(word);
        if (intHolder == null) {
            intHolder = new IntHolder();
            map.put(word, intHolder);
        }
        intHolder.increment();
    }

    public void remove(String word) {
        IntHolder intHolder = map.get(word);
        if (intHolder != null) {
            intHolder.decrement();
        }
    }

    public Map<String, Integer> getCounts() {
        return Maps.transformEntries(map, new Maps.EntryTransformer<String, IntHolder, Integer>() {
            public Integer transformEntry(String key, IntHolder value) {
                return value.get();
            }
        });
    }

    public void clear() {
        map.clear();
    }

    public boolean isEmpty() {
        return map.isEmpty();
    }

}
