package com.atlassian.stash.saucycharts.internal.rest;

import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.rest.interceptor.ResourceContextInterceptor;
import com.atlassian.stash.rest.util.ResourcePatterns;
import com.atlassian.stash.rest.util.ResponseFactory;
import com.atlassian.stash.rest.util.RestUtils;
import com.atlassian.stash.saucycharts.SaucyService;
import com.atlassian.stash.saucycharts.internal.rest.model.AuthorLoc;
import com.atlassian.stash.saucycharts.internal.rest.model.BlameSnapshot;
import com.atlassian.stash.saucycharts.internal.rest.model.WordCount;
import com.atlassian.stash.util.Pair;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.sun.jersey.spi.resource.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@InterceptorChain(ResourceContextInterceptor.class)
@Path(ResourcePatterns.REPOSITORY_URI)
@Produces({RestUtils.APPLICATION_JSON_UTF8})
@Singleton
public class SaucyResource {

    private static final Logger log = LoggerFactory.getLogger(SaucyResource.class);

    /**
     * Convert {"Tim Pettersen", 123} to {"author": "Tim Pettersen", "loc": 123}, say.
     */
    public static final Function<Map.Entry<String, Integer>, AuthorLoc> TO_AUTHOR_BLAME =
            new Function<Map.Entry<String, Integer>, AuthorLoc>() {
                @Override
                public AuthorLoc apply(Map.Entry<String, Integer> input) {
                    return new AuthorLoc(input.getKey(), input.getValue());
                }
            };

    /**
     * Convert {"Tim Pettersen", [123, -456]} to {"author": "Tim Pettersen", "added": 123, "removed": -456}, say.
     */
    public static final Function<Map.Entry<String, Pair<Integer, Integer>>, AuthorLoc> TO_AUTHOR_NUMSTAT =
            new Function<Map.Entry<String, Pair<Integer, Integer>>, AuthorLoc>() {
                @Override
                public AuthorLoc apply(Map.Entry<String, Pair<Integer, Integer>> input) {
                    Pair<Integer, Integer> pair = input.getValue();
                    return new AuthorLoc(input.getKey(), pair.getFirst(), pair.getSecond());
                }
            };

    /**
     * Convert {"interface", 123} to {"word": "interface", "count": 123}, say.
     */
    public static final Function<Map.Entry<String, Integer>, WordCount> TO_WORD_COUNT =
            new Function<Map.Entry<String, Integer>, WordCount>() {
                @Override
                public WordCount apply(Map.Entry<String, Integer> input) {
                    return new WordCount(input.getKey(), input.getValue());
                }
            };

    private final SaucyService saucyService;

    public SaucyResource(SaucyService saucyService) {
        this.saucyService = saucyService;
    }

    @GET
    @Path("loc-history/{path:.*}")
    public Response locHistory(@Context Repository repository, @PathParam("path") String path,
                               @QueryParam("at") @DefaultValue("HEAD") String at,
                               @QueryParam("segments") @DefaultValue("12") Integer segments) {
        Map<Long, Map<String, Pair<Integer, Integer>>> locHistory = saucyService.getLocHistory(repository, path, at, segments);

        // translate into pretty json structure
        List<BlameSnapshot> snapshots = Lists.newArrayList();
        for (Map.Entry<Long, Map<String, Pair<Integer, Integer>>> entry : locHistory.entrySet()) {
            List<AuthorLoc> authors = Lists.newArrayList(Iterables.transform(entry.getValue().entrySet(), TO_AUTHOR_NUMSTAT));
            snapshots.add(new BlameSnapshot(entry.getKey(), authors));
        }

        return ResponseFactory.ok(snapshots ).build();
    }

    @GET
    @Path("blame-history/{path:.*}")
    public Response blameHistory(@Context Repository repository, @PathParam("path") String path,
                                 @QueryParam("at") @DefaultValue("HEAD") String at,
                                 @QueryParam("segments") @DefaultValue("12") Integer segments) {
        Map<Long, Map<String, Integer>> blameHistory = saucyService.getBlameHistory(repository, path, at, segments);

        // translate into pretty json structure
        List<BlameSnapshot> snapshots = Lists.newArrayList();
        for (Map.Entry<Long, Map<String, Integer>> entry : blameHistory.entrySet()) {
            List<AuthorLoc> authors = Lists.newArrayList(Iterables.transform(entry.getValue().entrySet(), TO_AUTHOR_BLAME));
            snapshots.add(new BlameSnapshot(entry.getKey(), authors));
        }

        return ResponseFactory.ok(snapshots).build();
    }

    @GET
    @Path("blame/{path:.*}")
    public Response blame(@Context Repository repository, @PathParam("path") String path,
                          @QueryParam("at") @DefaultValue("HEAD") String at) {
        Map<String, Integer> blame = saucyService.getBlame(repository, path, at);
        return ResponseFactory.ok(Lists.newArrayList(Iterables.transform(blame.entrySet(), TO_AUTHOR_BLAME))).build();
    }

    @GET
    @Path("words/{path:.*}")
    public Response words(@Context Repository repository, @PathParam("path") String path,
                          @QueryParam("at") @DefaultValue("HEAD") String at) {
        Map<String, Integer> words = saucyService.getWords(repository, path, at);
        return ResponseFactory.ok(Lists.newArrayList(Iterables.transform(words.entrySet(), TO_WORD_COUNT))).build();
    }

}
