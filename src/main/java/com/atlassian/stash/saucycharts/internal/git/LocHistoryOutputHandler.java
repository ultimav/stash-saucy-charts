package com.atlassian.stash.saucycharts.internal.git;

import com.atlassian.stash.io.LineReader;
import com.atlassian.stash.io.LineReaderOutputHandler;
import com.atlassian.stash.saucycharts.internal.util.IntHolder;
import com.atlassian.stash.scm.CommandOutputHandler;
import com.atlassian.stash.util.Pair;
import com.google.common.base.Function;
import com.google.common.collect.Maps;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LocHistoryOutputHandler extends LineReaderOutputHandler implements CommandOutputHandler<Map<Long, Map<String, Pair<Integer, Integer>>>> {

    private static final Pattern AUTHOR = Pattern.compile("(.*) ([0-9]+)");
    private static final Pattern LOC = Pattern.compile("([0-9]+)\\s+([0-9]+)\\s+[^0-9].*");

    private static final Function<Pair<IntHolder, IntHolder>, Pair<Integer, Integer>> TO_INTS =
            new Function<Pair<IntHolder, IntHolder>, Pair<Integer, Integer>>() {
        public Pair<Integer, Integer> apply(Pair<IntHolder, IntHolder> input) {
            return Pair.newInstance(input.getFirst().get(), input.getSecond().get());
        }
    };

    private final Map<Long, Map<String, Pair<Integer, Integer>>> historicalLoc = Maps.newLinkedHashMap(); // chronological order
    private final Map<String, Pair<IntHolder, IntHolder>> authorLoc = Maps.newHashMap();

    private final int segments;

    private long segmentLength = -1;
    private long nextSegmentStart = -1;
    private String currentAuthor = null;

    public LocHistoryOutputHandler(int segments) {
        super("UTF-8");
        this.segments = segments;
    }

    @Nullable
    public Map<Long, Map<String, Pair<Integer, Integer>>> getOutput() {
        return historicalLoc;
    }

    @Override
    protected void processReader(LineReader lineReader) throws IOException {
        String line;
        while ((line = lineReader.readLine()) != null) {
            Matcher authorMatcher = AUTHOR.matcher(line);
            if (authorMatcher.matches()) {
                currentAuthor = authorMatcher.group(1);
                long timestamp = Long.parseLong(authorMatcher.group(2));
                if (segmentLength == -1) {
                    segmentLength = ((System.currentTimeMillis() / 1000) - timestamp) / segments;
                    if (segmentLength <= 0) {
                        throw new IllegalStateException("Earliest timestamp is >= than now!");
                    }
                    nextSegmentStart = timestamp + segmentLength;
                } else {
                    while (timestamp > nextSegmentStart) {
                        // save out current counts
                        historicalLoc.put(nextSegmentStart, Maps.newHashMap(Maps.transformValues(authorLoc, TO_INTS)));
                        nextSegmentStart += segmentLength;
                    }
                }
                continue;
            }

            Matcher locMatcher = LOC.matcher(line);
            if (locMatcher.matches()) {
                Pair<IntHolder, IntHolder> intHolders = authorLoc.get(currentAuthor);
                if (intHolders == null) {
                    intHolders = Pair.newInstance(new IntHolder(), new IntHolder());
                    authorLoc.put(currentAuthor, intHolders);
                }
                intHolders.getFirst().add(Integer.parseInt(locMatcher.group(1)));
                intHolders.getSecond().add(-Integer.parseInt(locMatcher.group(2)));
            }
        }
    }

}
