package com.atlassian.stash.saucycharts.internal.util;

import com.google.common.base.Function;

public class IntHolder {

    public static final Function<IntHolder, Integer> TO_INT = new Function<IntHolder, Integer>() {
        public Integer apply(IntHolder input) {
            return input.n;
        }
    };

    private int n;

    public void increment() {
        n++;
    }

    public void decrement() {
        n--;
    }

    public void add(int increment) {
        n += increment;
    }

    public int get() {
        return n;
    }

}
