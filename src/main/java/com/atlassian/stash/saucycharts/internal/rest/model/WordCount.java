package com.atlassian.stash.saucycharts.internal.rest.model;

import com.atlassian.stash.rest.data.RestMapEntity;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class WordCount extends RestMapEntity {

    public WordCount(String word, int count) {
        put("word", word);
        put("count", count);
    }

}
