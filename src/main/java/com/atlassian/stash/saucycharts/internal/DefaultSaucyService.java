package com.atlassian.stash.saucycharts.internal;

import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.saucycharts.SaucyService;
import com.atlassian.stash.saucycharts.internal.git.BlameAttributionOutputHandler;
import com.atlassian.stash.saucycharts.internal.git.CatFileWordOutputHandler;
import com.atlassian.stash.saucycharts.internal.git.LocHistoryOutputHandler;
import com.atlassian.stash.saucycharts.internal.git.SegmentHistoryOutputHandler;
import com.atlassian.stash.scm.git.GitScm;
import com.atlassian.stash.throttle.ThrottleService;
import com.atlassian.stash.throttle.Ticket;
import com.atlassian.stash.util.Pair;
import com.google.common.collect.Maps;

import java.util.Map;

public class DefaultSaucyService implements SaucyService {

    private static final String SCM_HOSTING = "scm-hosting";

    private final GitScm gitScm;
    private final ThrottleService throttleService;

    public DefaultSaucyService(GitScm gitScm, ThrottleService throttleService) {
        this.gitScm = gitScm;
        this.throttleService = throttleService;
    }

    public Map<String, Integer> getWords(Repository repository, String path, String at) {
        Ticket ticket = throttleService.acquireTicket(SCM_HOSTING);        
        try {
            return gitScm.getCommandBuilderFactory()
                    .builder(repository)
                    .command("cat-file")
                    .argument("-p")
                    .argument(at + ":" + path)
                    .build(new CatFileWordOutputHandler())
                    .call();
        } finally {
            ticket.release();
        }
    }

    public Map<String, Integer> getBlame(Repository repository, String path, String at) {
        Ticket ticket = throttleService.acquireTicket(SCM_HOSTING);
        try {
            return getBlameHelper(repository, path, at);
        } finally {
            ticket.release();
        }
    }

    private Map<String, Integer> getBlameHelper(Repository repository, String path, String at) {
        return gitScm.getCommandBuilderFactory()
                .builder(repository)
                .blame()
                .incremental()
                .file(path)
                .rev(at)
                .build(new BlameAttributionOutputHandler())
                .call();
    }

    public Map<Long, Map<String, Integer>> getBlameHistory(Repository repository, String path, String at, int segments) {
        Ticket ticket = throttleService.acquireTicket(SCM_HOSTING);
        try {
            Map<Long, String> historySegments = gitScm.getCommandBuilderFactory()
                    .builder(repository)
                    .command("log")
                    .argument("--reverse") // is this super exy?
                    .argument("--format=%at %H")
                    .argument("--diff-filter=AM") // only show adds & modifications (renames/deletes break things) // TODO is this kosher?
                    .argument(at)
                    .argument("--")
                    .argument(path)
                    .build(new SegmentHistoryOutputHandler(12))
                    .call();

            Map<Long, Map<String, Integer>> blameHistory = Maps.newLinkedHashMap(); // chronological order
            for (Map.Entry<Long, String> entry : historySegments.entrySet()) {
                blameHistory.put(entry.getKey(), getBlame(repository, path, entry.getValue()));
            }
            return blameHistory;
        } finally {
            ticket.release();
        }
    }

    public Map<Long, Map<String, Pair<Integer, Integer>>> getLocHistory(Repository repository, String path, String at, int segments) {
        Ticket ticket = throttleService.acquireTicket(SCM_HOSTING);
        try {
            return gitScm.getCommandBuilderFactory()
                    .builder(repository)
                    .command("log")
                    .argument("--reverse") // is this super exy?
                    .argument("--format=%an %at")
                    .argument("--numstat")
                    .argument(at)
                    .argument("--")
                    .argument(path)
                    .build(new LocHistoryOutputHandler(segments))
                    .call();
        } finally {
            ticket.release();
        }
    }
}
