package com.atlassian.stash.saucycharts.internal.git;

import com.atlassian.stash.io.LineReader;
import com.atlassian.stash.io.LineReaderOutputHandler;
import com.atlassian.stash.scm.CommandOutputHandler;
import com.google.common.collect.Maps;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SegmentHistoryOutputHandler extends LineReaderOutputHandler implements CommandOutputHandler<Map<Long, String>> {

    private static final Pattern COMMIT = Pattern.compile("([0-9]+) ([0-9a-f]{40})");

    private final Map<Long, String> commits = Maps.newLinkedHashMap(); // chronological order

    private final int segments;

    private long segmentLength = -1;
    private long nextSegmentStart = -1;

    private long lastTimestamp = -1;
    private String lastCommit = null;

    public SegmentHistoryOutputHandler(int segments) {
        super("UTF-8");
        this.segments = segments;
    }

    @Nullable
    public Map<Long, String> getOutput() {
        return commits;
    }

    @Override
    protected void processReader(LineReader lineReader) throws IOException {
        String line;
        while ((line = lineReader.readLine()) != null) {
            Matcher m = COMMIT.matcher(line);
            if (m.matches()) {
                lastTimestamp = Long.parseLong(m.group(1));
                lastCommit = m.group(2);

                if (segmentLength == -1) {
                    // first commit - setup segment lengths
                    segmentLength = ((System.currentTimeMillis() / 1000) - lastTimestamp) / (segments - 1);
                    if (segmentLength <= 0) {
                        throw new IllegalStateException("Earliest timestamp is >= than now!");
                    }
                    nextSegmentStart = lastTimestamp + segmentLength;
                    commits.put(lastTimestamp, lastCommit);
                } else {
                    // commit has passed the boundary of this segment, store it
                    if (lastTimestamp > nextSegmentStart) {
                        while (lastTimestamp > nextSegmentStart) {
                            // mark commit as segment start
                            commits.put(nextSegmentStart, lastCommit);
                            nextSegmentStart += segmentLength;
                        }
                        lastCommit = null;
                    }
                }
            }
        }

        // store the last commit too, if it hasn't been stored
        if (lastCommit != null) {
            commits.put(lastTimestamp, lastCommit);
        }
    }

}
