package com.atlassian.stash.saucycharts.internal.rest;

import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.rest.exception.UnhandledExceptionMapper;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class SaucyExceptionMapper extends UnhandledExceptionMapper {

    public SaucyExceptionMapper(NavBuilder navBuilder) {
        super(navBuilder);
    }

}
