package com.atlassian.stash.saucycharts.internal.git;

import com.atlassian.stash.io.LineReader;
import com.atlassian.stash.io.LineReaderOutputHandler;
import com.atlassian.stash.saucycharts.internal.util.HashMapWordCounter;
import com.atlassian.stash.saucycharts.internal.util.WordCounter;
import com.atlassian.stash.scm.CommandOutputHandler;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.Map;

public class CatFileWordOutputHandler extends LineReaderOutputHandler implements CommandOutputHandler<Map<String, Integer>> {

    private static final String DELIMITERS = "[^\\w]+";

    private final WordCounter wordCounts = new HashMapWordCounter();

    public CatFileWordOutputHandler() {
        super("UTF-8");
    }

    public Map<String, Integer> getOutput() {
        return wordCounts.getCounts();
    }

    @Override
    protected void processReader(LineReader lineReader) throws IOException {
        String line;
        while ((line = lineReader.readLine()) != null) {
            for (String token : line.split(DELIMITERS)) {
                if (!StringUtils.isEmpty(token)) {
                    wordCounts.add(token);
                }
            }
        }
    }

}
