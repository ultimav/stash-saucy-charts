package com.atlassian.stash.saucycharts.internal.rest.model;

import com.atlassian.stash.rest.data.RestMapEntity;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize
public class BlameSnapshot extends RestMapEntity {

    public BlameSnapshot(long timestamp, List<AuthorLoc> blame) {
        put("timestamp", timestamp);
        put("blame", blame);
    }

}
